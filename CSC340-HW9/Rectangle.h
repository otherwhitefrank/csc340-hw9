//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Rectangle class inherits Figure in our hypothetical graphic system


#ifndef _RECTANGLE_H
#define _RECTANGLE_H

#include "figure.h"
#include <iostream>

class Rectangle : public Figure
{
public:

	void erase(); //Override erase in Rectangle class
	void draw();  //Override draw in Rectangle class

	Rectangle(void);
	~Rectangle(void);
};

#endif
