//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Driver for out hypothetical graphic system

#include <iostream>
#include "figure.h"
#include "rectangle.h"
#include "triangle.h"

using namespace std;

int main()
{
	int temp = 0;

	Triangle tri;
	tri.draw();
	cout << "\nDerived class Triangle object calling" << " center().\n";
	tri.center();

	Rectangle rect;
	rect.draw();
	cout << "\nDerived class Rectangle object calling" << " center().\n";
	rect.center();

	cin >> temp;
	return 0;
}