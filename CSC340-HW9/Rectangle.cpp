//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Rectangle class inherits Figure in our hypothetical graphic system

#include "Rectangle.h"


Rectangle::Rectangle(void)
{
}


Rectangle::~Rectangle(void)
{
}



void Rectangle::erase() //Override erase() in triangle
{
	cout << "Calling Rectangle::erase() " << "\n";
}

void Rectangle::draw() //Override draw() in triangle
{

	cout << "Calling Rectangle::draw() " << "\n";
}