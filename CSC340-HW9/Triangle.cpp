//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Triangle class inherits Figure in our hypothetical graphic system


#include "Triangle.h"


Triangle::Triangle(void)
{
}


Triangle::~Triangle(void)
{
}


void Triangle::erase() //Override erase() in triangle
{
	cout << "Calling Triangle::erase() " << "\n";
}

void Triangle::draw() //Override draw() in triangle
{

	cout << "Calling Triangle::draw() " << "\n";
}