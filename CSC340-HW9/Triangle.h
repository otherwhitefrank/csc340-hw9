//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Triangle class inherits Figure in our hypothetical graphic system

#ifndef _TRIANGLE_H
#define _TRIANGLE_H

#include "figure.h"
#include <iostream>

class Triangle : public Figure
{
public:

	void erase(); //Override erase() in triangle
	void draw(); //Override draw() in triangle

	Triangle(void);
	~Triangle(void);
};

#endif
