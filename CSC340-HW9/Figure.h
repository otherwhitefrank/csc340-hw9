//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Base "Figure" class that our hypothetical graphic system inherits 

#ifndef _FIGURE_H
#define _FIGURE_H


#include <iostream>

using namespace std;

class Figure
{
public:


	virtual void erase(); //Erase a figure
	virtual void draw();  //Draw a figure
	void center();


	Figure(void);
	~Figure(void);
};


#endif