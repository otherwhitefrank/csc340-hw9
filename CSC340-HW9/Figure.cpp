//Name: Frank Dye
//ID: 912927332
//Date: 12/9/13
//Description: Base "Figure" class that our hypothetical graphic system inherits 


#include "Figure.h"


Figure::Figure(void)
{

}


Figure::~Figure(void)
{
}

void Figure::draw()
{
	cout << "Calling Figure::erase() " << "\n";
}

void Figure::erase()
{
	cout << "Calling Figure::draw() " << "\n";
}

void Figure::center()
{
	erase();
	draw();
}